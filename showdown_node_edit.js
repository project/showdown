(function ($) {

/**
 * Live markdown preview
 */
Drupal.behaviors.showdown = {
  attach: function (context, settings) {
    $('#edit-body textarea', context).keyup(function () {
      var converter = new Showdown.converter();
      $('#showdown_preview').html(converter.makeHtml($(this).val()));
    });
  }
};

})(jQuery);

