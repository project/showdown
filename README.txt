In order for this module to work correctly, you need to clone
https://github.com/cweagans/showdown into the module directory.

Example:

cd sites/all/modules/showdown
git clone https://github.com/cweagans/showdown.git

